FROM openjdk:11-jre
#Открываем порт в docker container
EXPOSE 8989
#Создаем переменную которая хранит путь до jar файла
ARG JAR_FILE=build/libs/gateway-1.0.jar
#Копируем jar файл в docker container
ADD ${JAR_FILE} gateway.jar
#Запускам jar файл
ENTRYPOINT ["java","-jar","gateway.jar"]