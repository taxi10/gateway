package com.test.gateway.openfeign;

import db.entity.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "company", url = "company:8032/company/")
public interface DepartmentApi {

    @GetMapping(value = "authentication")
    Driver getDepartmentByEmail(@RequestParam(value = "email") String email);

}
