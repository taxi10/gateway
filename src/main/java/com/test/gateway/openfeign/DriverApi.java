package com.test.gateway.openfeign;

import db.entity.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "driver", url = "driver:8081/driver/")
public interface DriverApi {

    @GetMapping(value = "authentication")
    Driver getDriverByEmail(@RequestParam(value = "email") String email);

}
