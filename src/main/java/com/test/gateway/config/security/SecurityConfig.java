package com.test.gateway.config.security;

import com.test.gateway.service.UserDetailsCustom;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.savedrequest.NullRequestCache;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsCustom userDetailsCustom;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsCustom::loadUserByUsername)
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Включаем .http Basic() для тестирования через postman
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/driver/registration").permitAll()
                .antMatchers("/driver/**").hasRole("DRIVER")
                .antMatchers("/company/registration").permitAll()
                .antMatchers("/company/**").hasRole("COMPANY")
                .antMatchers("/order/**").hasAnyRole("COMPANY", "DRIVER")
                .antMatchers("/login").anonymous()
                .and().formLogin().and().httpBasic();

        http
                .requestCache()
                .requestCache(new NullRequestCache());

        http
                .logout()
                .deleteCookies("MYSESSION")
                .clearAuthentication(true)
                .invalidateHttpSession(true);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
