package com.test.gateway.config.web;

import com.test.gateway.config.web.servlet.ServletHeader;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class WebConfig {

    @Bean
    public ServletRegistrationBean servletPrincipalUser() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new ServletHeader(), "");
        return registrationBean;
    }


}
