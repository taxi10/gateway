package com.test.gateway.service;

import com.test.gateway.openfeign.DepartmentApi;
import com.test.gateway.openfeign.DriverApi;
import db.entity.Driver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsCustom implements UserDetailsService {

    private final DepartmentApi departmentApi;

    private final DriverApi driverApi;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        if (email.endsWith("@yandex.ru")) {
            Driver departmentByEmail = departmentApi.getDepartmentByEmail(email);
            return User.builder()
                    .username(departmentByEmail.getEmail())
                    .password(departmentByEmail.getPassword())
                    .roles(departmentByEmail.getRole().getRole())
                    .build();
        }

        Driver driverByEmail = driverApi.getDriverByEmail(email);

        return User.builder()
                .username(driverByEmail.getEmail())
                .password(driverByEmail.getPassword())
                .roles(driverByEmail.getRole().getRole())
                .build();
    }
}
